-The potential contribution of Geoscripting for yourself.
This course will allow us to get some basic skills regarding scripting and combining this with visualisations like maps. Doing this course will be able to make certain processes more efficient  

-The specific R or Python knowledge you would need to learn about.
We want to be able to write certain functions that for example can extract certain data out of a bigger dataset. Or other kinds of functions that can help us analyzing data. 

-Describe the data you would like to use for your final project
-Is the data available?
Sentinel-2-data of the Netherlands 

-What type of data would you like to visualise/analyse?
Yes, there is a lot of Sentinel-2-data available
